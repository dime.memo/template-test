## :loudspeaker: Type of change
<!--- Put an `x` in the boxes that apply -->
- [ ] Bugfix
- [ ] New feature
- [ ] Enhancement
- [ ] Refactoring
- [ ] Release

## :scroll: Motivation and Context
<!--- Why is this change required? What problem does it solve? -->
<!--- If it fixes an open issue, please link to the issue here. -->
<!--- Describe your changes in detail -->


## :art: UI changes
<!--- Screenshots, images, videos showcasing and Mandatory for UI changes -->
<!--- <img src="https://your_image_url" width="260">&emsp;<img src="https://your_image_url" width="260"> -->

<!-- 
 Before | After
--- | --- 
<img src="https://your_image_url" width="260"> | <img src="https://your_image_url" width="260">
 -->


## :green_heart: How did you test it?
<!--- Describe How did you test the changes -->


## :pushpin: Related ticket(s)
<!--- Link to JIRA ticket - Title of JIRA ticket -->
